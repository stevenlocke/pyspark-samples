import os
from pyspark.sql import SparkSession, functions
from pyspark.sql.types import StructType, StringType, IntegerType, StructField
from _db_init import db_properties, db_url

test_file = "pipe_delim.txt"
spark = SparkSession \
        .builder \
        .appName("LoadTestFile") \
        .config("spark.jars", os.getenv("SPARK_CLASSPATH")) \
        .getOrCreate()
schema = StructType(
    [
        StructField("practiceid", IntegerType(), False),
        StructField("title", StringType(), False),
        StructField("bpracticeid", IntegerType(), False),
        StructField("taxid", IntegerType(), False),
        StructField("bbusinessunitid", IntegerType(), True),
    ]
)
df = spark.read.csv(test_file, schema=schema, sep="|")
df.withColumn("created_at", functions.current_timestamp())
df.withColumn("updated_at", functions.current_timestamp())
print("show data frame")
df.show()

print("write data frame to table")
df.write.saveAsTable(
    name="practice",
    mode="overwrite",
    format="jdbc",
    dbtable="practice",
    **db_properties
)

del df

print("read database table into another frame")
d = spark.read.jdbc(url=db_url, table="practice", properties=db_properties)
print("show data frame from database")
d.show()

del d

test_file = "Accidental_Drug_Related_Deaths_2012-2018.csv"
print("load accidental drug related deaths 2012-2018")
df = spark.read.csv(test_file, header=True)
df.withColumn("created_at", functions.current_timestamp())
df.withColumn("updated_at", functions.current_timestamp())
print("show accidental deaths data frame")
df.show()

print("write data frame to table")
df.write.saveAsTable(
    name="accidental_deaths",
    mode="overwrite",
    format="jdbc",
    dbtable="accidental_deaths",
    **db_properties
)

del df

test_file = "Number_of_Drug_and_Alcohol-Related_Intoxication_Deaths_by_Place_of_Occurrence__2007-2016_1__2_.csv"
print("number of drug and alcohol related intoxication deaths by place of occurrance 2007-2016.csv")
df = spark.read.csv(test_file, header=True)
df.withColumn("created_at", functions.current_timestamp())
df.withColumn("updated_at", functions.current_timestamp())
print("show data frame")
df.show()

print("write data frame to table")
df.write.saveAsTable(
    name="drug_alcohol_deaths",
    mode="overwrite",
    format="jdbc",
    dbtable="drug_alcohol_deaths",
    **db_properties
)

del df

test_file = "Opiate_Overdoses_by_Age_Range__Gender__and_Drug_Type_FY_2018.csv"
print("opiate overdoses by age range, gender, drug type 2018")
df = spark.read.csv(test_file, header=True)
df.withColumn("created_at", functions.current_timestamp())
df.withColumn("updated_at", functions.current_timestamp())
print("show data frame")
df.show()

print("write data frame to table")
df.write.saveAsTable(
    name="opiate_overdoses_by_age_range_gender_drug_type",
    mode="overwrite",
    format="jdbc",
    dbtable="opiate_overdoses_by_age_range_gender_drug_type",
    **db_properties
)

del df

spark.stop()
