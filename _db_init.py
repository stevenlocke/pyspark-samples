import configparser

#Create the Database properties
db_properties={}
config = configparser.ConfigParser()
config.read("config.ini")
db_prop = config['postgresql']
db_url = db_prop['url']
db_properties['url']=db_prop['url']
db_properties['user']=db_prop['username']
db_properties['password']=db_prop['password']
db_properties['driver']=db_prop['driver']
db_properties['schema']=db_prop['schema']
