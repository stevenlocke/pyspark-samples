"""
NOTE: This data processing isn't valid. Just tinkering
"""
import os
from pyspark.sql import SparkSession
from _db_init import db_properties, db_url

spark = SparkSession \
        .builder \
        .appName("LoadTestFile") \
        .config("spark.jars", os.getenv("SPARK_CLASSPATH")) \
        .getOrCreate()

print("read database table into frame")
dad = spark.read.jdbc(url=db_url, table="some_table", properties=db_properties)
print("show data frame from database")
dad.show()
