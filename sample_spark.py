from pyspark.sql import SparkSession

spark = SparkSession \
        .builder \
        .appName("getEvenNums") \
        .getOrCreate()

sc = spark.sparkContext
x = sc.parallelize([1, 2, 3, 4])
y = x.filter(lambda x: (x % 2 == 0))
print(y.collect())
spark.stop()
